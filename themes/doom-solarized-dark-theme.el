;;; doom-solarized-dark-theme.el --- inspired by Atom One Dark

(require 'doom-themes)
(message "loading solarized-dark")

(defgroup doom-solarized-dark-theme nil
  "Options for doom-themes"
  :group 'doom-themes)

(defcustom doom-solarized-dark-brighter-modeline nil
  "If non-nil, more vivid colors will be used to style the mode-line."
  :group 'doom-solarized-dark-theme
  :type 'boolean)

(defcustom doom-solarized-dark-brighter-comments nil
  "If non-nil, comments will be highlight in more vivid colors."
  :group 'doom-solarized-dark-theme
  :type 'boolean)

(defcustom doom-solarized-dark-comment-bg doom-solarized-dark-brighter-comments
  "If non-nil, comments will have a subtle, darker background. Enhancing their
legibility."
  :group 'doom-solarized-dark-theme
  :type 'boolean)

(defcustom doom-solarized-dark-padded-modeline nil
  "If non-nil, adds a 4px padding to the mode-line. Can be an integer to
determine the exact padding."
  :group 'doom-solarized-dark-theme
  :type '(or integer boolean))

(defun solarized (colorkey)
  "Retrieves a Solazired color for COLORKEY as defined in the Solarized spec."
  (let ((color-map
         ;; name      default   256       16
         '(base03    ("#002b36" "#1c1c1c" "brightblack")
           base02    ("#073642" "#262626" "black")
           base01    ("#586e75" "#585858" "brightgreen")
           base00    ("#657b83" "#626262" "brightyellow")
           base0     ("#839496" "#808080" "brightblue")
           base1     ("#93a1a1" "#8a8a8a" "brightcyan")
           base2     ("#eee8d5" "#e4e4e4" "white")
           base3     ("#fdf6e3" "#ffffd7" "brightwhite")
           yellow    ("#b58900" "#af8700" "yellow")
           orange    ("#cb4b16" "#d75f00" "brightred")
           red       ("#dc322f" "#d70000" "red")
           magenta   ("#d33682" "#af005f" "magenta")
           violet    ("#6c71c4" "#5f5faf" "brightmagenta")
           blue      ("#268bd2" "#0087ff" "blue")
           cyan      ("#2aa198" "#00afaf" "cyan")
           green     ("#859900" "#5f8700" "green"))))
    (plist-get color-map colorkey)))

(def-doom-theme doom-solarized-dark
  "A dark theme inspired by Solarized dark"
  (
   ;; colors required by doom-common-themes.el
   (red       (solarized 'red))
   (orange    (solarized 'orange))
   (green     (solarized 'green))
   (teal      (doom-lighten (solarized 'cyan) 0.25))
   (yellow    (solarized 'yellow))
   (blue      (solarized 'blue))
   (dark-blue (doom-lighten (solarized 'base02) 0.1))
   (magenta   (solarized 'magenta))
   (violet    (solarized 'violet))
   (cyan      (solarized 'cyan))
   (dark-cyan (doom-darken (solarized 'cyan) 0.1))
   (grey      (solarized 'base00))

   ;;
   (bg     (solarized 'base03))
   (bg-alt (solarized 'base02))
   (fg     (solarized 'base0))
   (fg-alt (solarized 'base1))
   (base0  (solarized 'base03))
   (base1  (solarized 'base02))
   (base2  (doom-lighten base1 0.02))
   (base3  (doom-lighten base2 0.02))
   (base4  (doom-lighten base3 0.05))
   (base5  (solarized 'base01))
   (base6  (solarized 'base00))
   (base7  (solarized 'base0))
   (base8  (solarized 'base1))

   ;; face categories -- required for all themes
   (highlight      blue)
   (vertical-bar   (doom-darken base1 0.1))
   (selection      blue)
   (builtin        yellow)
   (comments       (if doom-solarized-dark-brighter-comments base8 base5))
   (doc-comments   (doom-lighten (if doom-solarized-dark-brighter-comments base8 base5) 0.5))
   (constants      violet)
   (functions      green)
   (keywords       blue)
   (methods        cyan)
   (operators      blue)
   (type           yellow)
   (strings        cyan)
   (variables      fg)
   (numbers        orange)
   (region         base2)
   (error          red)
   (warning        yellow)
   (success        green)
   (vc-modified    blue)
   (vc-added       green)
   (vc-deleted     red)

   ;; custom categories
   (hidden     `(,(car bg) "black" "black"))
   (-modeline-bright doom-solarized-dark-brighter-modeline)
   (-modeline-pad
    (when doom-solarized-dark-padded-modeline
      (if (integerp doom-solarized-dark-padded-modeline) doom-solarized-dark-padded-modeline 4)))

   (modeline-fg     nil)
   (modeline-fg-alt base4)

   (modeline-bg
    (if -modeline-bright
        (doom-lighten bg-alt 0.1)
      bg-alt))

   (modeline-bg-solaire
    (if -modeline-bright
        (doom-darken blue 0.45)
      `(,(doom-darken (car bg-alt) 0.05) ,@(cdr base0))))

   (modeline-bg-inactive   (doom-darken bg-alt 0.02))
   (modeline-bg-inactive-solaire `(,(car bg-alt) ,@(cdr base1))))

  ;; --- extra faces ------------------------
  ((elscreen-tab-other-screen-face :background "#353a42" :foreground "#1e2022")
   ((line-number &override) :foreground base2)
   ((line-number-current-line &override) :foreground fg)

   ((region &override) :foreground base0)

   ;;(font-lock-comment-face
   ;; :foreground comments
   ;; :background bg)
   (font-lock-doc-face
    :inherit 'font-lock-comment-face
    :foreground doc-comments)
   ((font-lock-type-face &override) :slant 'italic)
   ((font-lock-builtin-face &override) :slant 'italic)
   ((font-lock-function-name-face &override) :weight 'semi-bold)
   ((font-lock-keyword-face &override) :weight 'semi-bold )

   ((ivy-current-match &override)  :background highlight :foreground base0)
   ((ivy-virtual &override)  :foreground base2 :italic nil)
   ((lazy-highlight &override) :background magenta :foreground base0)

   (doom-modeline-bar :background (if -modeline-bright modeline-bg highlight))

   (mode-line
    :background modeline-bg :foreground modeline-fg
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg)))

   (mode-line-inactive
    :background modeline-bg-inactive :foreground modeline-fg-alt
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-inactive)))

   (solaire-mode-line-face
    :inherit 'mode-line
    :background modeline-bg-solaire
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-solaire)))
   (solaire-mode-line-inactive-face
    :inherit 'mode-line-inactive
    :background modeline-bg-inactive-solaire
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-inactive-solaire)))

   ((company-tooltip-selection &override) :foreground bg)
   ((company-tooltip-common &override) :foreground magenta)
   ((company-tooltip-common-selection &override) :foreground bg)

   ;; --- major-mode faces -------------------
   ;; css-mode / scss-mode
   (css-proprietary-property :foreground orange)
   (css-property             :foreground green)
   (css-selector             :foreground blue)

   ;; markdown-mode
   (markdown-markup-face :foreground base5)
   (markdown-header-face :inherit 'bold :foreground red)
   (markdown-code-face :background (doom-lighten base3 0.05))

   ;; org-mode
   (org-hide :foreground bg)
   (org-block :background bg-alt)
   ((org-level-1 &override) :background bg-alt)
   ((org-level-2 &override) :background bg :foreground green)
   ((org-level-3 &override) :foreground fg)

   ;;ivy
   ((ivy-virtual &override)  :foreground base5))


  ;;--- extra variables ---------------------
  ;;()
  )

;;; doom-solarized-dark-theme.el ends here
